import {Component} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent {
  title = '';
  description = '';

  constructor(public http: HttpClient) { }

  createPost() {
    const title: string = this.title;
    const description: string = this.description;

    const body = {title, description};
    this.http.post('https://plovo-cc061-default-rtdb.firebaseio.com/posts.json', body).subscribe();
  }

}
