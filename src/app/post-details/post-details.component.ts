import { Component, OnInit } from '@angular/core';
import {Post} from '../shared/post.model';
import {ActivatedRoute, Params} from '@angular/router';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.css']
})
export class PostDetailsComponent {
  post!: Post;

  constructor(
    private route: ActivatedRoute,
    private http: HttpClient,
  ) { }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      const postId = params['id'];
      this.http.get('https://plovo-cc061-default-rtdb.firebaseio.com/posts/' + postId + '.json')
        .subscribe()
    });
  }

}
