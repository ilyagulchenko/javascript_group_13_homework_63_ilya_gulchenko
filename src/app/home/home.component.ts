import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs';
import {Post} from '../shared/post.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  posts!: Post[];

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.http.get<{[id: string]: Post}>('https://plovo-cc061-default-rtdb.firebaseio.com/posts.json')
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const postData = result[id];

          return new Post(
            id,
            postData.title,
            postData.description,
          );
        });
      }))
      .subscribe(posts => {
        this.posts = posts;
      });
  }

}
